# VEGA-ARDUINO

Source code, tools and scripts for VEGA processor based boards to be supported on Arduino IDE

## Getting started

In the Arduino IDE, the "Additional Boards Manager URLs" field allows users to add URLs that point to JSON files containing board definitions. These JSON files contain information about additional microcontroller boards that are not included by default in the Arduino IDE. By adding these URLs, users can expand the range of supported boards beyond the default set, enabling them to work with various third-party or community-developed boards.

## How to Add Additional Board URLs

#### 1. Open the Arduino IDE:

- Launch the Arduino IDE on your computer.

#### 2. Open Preferences:

- Go to File > Preferences.

#### 3. Locate the "Additional Boards Manager URLs" Field:

- In the Preferences window, find the field labeled "Additional Boards Manager URLs."

#### 4. Add URLs:

- Copy and paste the URLs of the JSON files for the additional boards you want to use into this field. If you have multiple URLs, separate them with commas.

Example URLs:

- https://gitlab.com/riscv-vega/vega-arduino/-/raw/main/package_vega_index.json?ref_type=heads

- https://dl.espressif.com/dl/package_esp8266_index.json

#### 5. Close Preferences:

- Click "OK" to save your changes and close the Preferences window.

#### 6. Open the Boards Manager:

- Go to Tools > Board > Boards Manager...

#### 7. Install the Additional Boards:

- In the Boards Manager, search for the additional boards you added. You should see them listed. Click "Install" to add the board definitions to your IDE.

## Adding support for VEGA ARIES Boards 

If you want to add support for VEGA ARIES boards, you would add the following URL to the "Additional Boards Manager URLs" field:
```vega
https://gitlab.com/riscv-vega/vega-arduino/-/raw/main/package_vega_index.json?ref_type=heads
```
After adding the URL and installing the VEGA ARIES boards via the Boards Manager, you can select an VEGA ARIES boards from the Tools > Board menu and start programming it with the Arduino IDE.

These URLs allow you to easily expand the capabilities of your Arduino IDE to support a wide variety of microcontroller boards.